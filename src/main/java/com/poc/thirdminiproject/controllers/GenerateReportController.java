package com.poc.thirdminiproject.controllers;


import com.poc.thirdminiproject.excpetions.ReportGenerationException;
import com.poc.thirdminiproject.request.ReportGenerationRequestBinding;
import com.poc.thirdminiproject.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Controller
public class GenerateReportController {

    @Autowired
    ReportService reportService;




    /*
    make this type request in postman
    {
    "mobileCatalog":[
        {
            "name" : "F14",
            "brand" : "Samsung",
            "price" : "234",
             "ram" : "3 GB"
        },
        {
            "name" : "Z2 PLUS",
            "brand" : "LENOVO",
            "price" : "2349990",
             "ram" : "4 GB"
        },
         {
            "name" : "A5",
            "brand" : "NOKIA",
            "price" : "2349990",
             "ram" : "4 GB"
        }
    ]
}

     */

    @PostMapping(value = "/report/pdf/", consumes = "application/json", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<byte[]> generatePdfReport(@RequestBody ReportGenerationRequestBinding request) throws ReportGenerationException {


        byte[] contents = reportService.generatePdfReport(request.getMobileCatalog());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);

        String filename = "Mobile Catalog.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);

    }

    @PostMapping(value = "/report/excel/", consumes = "application/json", produces = "application/xls")
    public ResponseEntity<byte[]> generateExcelReport(@RequestBody ReportGenerationRequestBinding request) throws ReportGenerationException {


        byte[] contents = reportService.generateExcelReport(request.getMobileCatalog());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=mobiles.xlsx");

        return new ResponseEntity<>(contents, headers, HttpStatus.OK);

    }


}
