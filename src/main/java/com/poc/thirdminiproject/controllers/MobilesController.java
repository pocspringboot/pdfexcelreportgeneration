package com.poc.thirdminiproject.controllers;

import com.poc.thirdminiproject.entitys.Mobile;
import com.poc.thirdminiproject.request.MobilesFilter;
import com.poc.thirdminiproject.services.MobileInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class MobilesController {

    @Autowired
    MobileInfoService mobileInfoService;


    //this annotation is use to load data in ui on each page
    @ModelAttribute
    public void loadData(Model model) {


        model.addAttribute("brands", mobileInfoService.getBrands());
        model.addAttribute("rams", mobileInfoService.getRams());
        model.addAttribute("ratings", mobileInfoService.getRatings());


    }

    @GetMapping("/welcome")
    public String initializePageWithOptions(Model model) {
        MobilesFilter mobilesFilter = new MobilesFilter();
        model.addAttribute("formObj", mobilesFilter);


        return "index";
    }

    // here  @ModelAttribute("formObj") is used to get data from ui as well as it is being sent back to ui
    @PostMapping("/searchMobiles")
    public String searchMobiles(@ModelAttribute("formObj") MobilesFilter formObj, Model model) {

        Integer pageNo=1;
        Integer pageSize=1;


        List<Mobile> mobiles = mobileInfoService.getMobilesBasedOnFilters(formObj);
        model.addAttribute("mobileList", mobiles);
        System.out.println(formObj);

        return "index";

    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


}
