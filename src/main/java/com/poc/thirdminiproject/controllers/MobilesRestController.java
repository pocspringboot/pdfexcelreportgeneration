package com.poc.thirdminiproject.controllers;

import com.poc.thirdminiproject.entitys.Mobile;
import com.poc.thirdminiproject.request.MobilesFilter;
import com.poc.thirdminiproject.services.MobileInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MobilesRestController {

    private static final Logger logger= LoggerFactory.getLogger(MobilesRestController.class);
    @Autowired
    MobileInfoService mobileInfoService;


    @PostMapping("/getMobilesByFilter")
    List<Mobile> getMobiles(@RequestBody MobilesFilter mobilesFilter) {

        logger.info("Filter Passed is {}",mobilesFilter);

        return mobileInfoService.getMobilesBasedOnFilters(mobilesFilter);

    }

    @GetMapping("/getAll")
    List<Mobile> getAllMobiles() {

        return mobileInfoService.getAllMobiles();

    }
}
