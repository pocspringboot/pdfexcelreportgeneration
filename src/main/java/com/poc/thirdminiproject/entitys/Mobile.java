package com.poc.thirdminiproject.entitys;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Mobile {


    @Id
    Integer id;
    String brand;
    Integer discount;
    String name;
    Integer price;
    Integer ram;
    Integer ratings;



}
