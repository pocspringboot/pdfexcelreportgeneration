package com.poc.thirdminiproject.pojos;

import lombok.Data;

@Data
public class MobileInfo {
    String price;
    String  ram;
    String brand;
    String name;
}
