package com.poc.thirdminiproject.repos;

import com.poc.thirdminiproject.entitys.Mobile;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface ProductRepo extends JpaRepository<Mobile, Serializable> {

//// Use can also use place holders like  in this
//       @Query("select mobiles from Mobile mobiles where (:brand is null or mobiles.brand = ?1)  and ( :ram is null or mobiles.ram = ?2) and (  :price is null or mobiles.price <= ?3) and (:rating is null or mobiles.ratings = ?4) ")
//    List<Mobile> getMobiles( String brand, Integer ram, Integer price, Integer rating);


//
//       @Query("select mobiles from Mobile mobiles where (:brand is null or mobiles.brand = :brand)  and ( :ram is null or mobiles.ram = :ram) and (  :price is null or mobiles.price <= :price) and (:rating is null or mobiles.ratings = :rating) ")
//    List<Mobile> getMobiles( @Param("brand") String brand, @Param("ram") Integer ram,@Param("price") Integer price,@Param("rating") Integer rating);
//


    //this one also works fine without @Param Annotations.

    @Query("select mobiles from Mobile mobiles where (:brand is null or mobiles.brand = :brand)  and ( :ram is null or mobiles.ram = :ram) and (  :price is null or mobiles.price <= :price) and (:rating is null or mobiles.ratings = :rating)")
    List<Mobile> getMobiles(String brand, Integer ram, Integer price, Integer rating);

    @Query("select distinct(ratings)  from Mobile ")
    List<Integer> getRatings();


    @Query("select distinct(brand)  from Mobile ")
    List<String> getBrands();


    @Query("select distinct(ram)  from Mobile ")
    List<Integer> getRams();


}
