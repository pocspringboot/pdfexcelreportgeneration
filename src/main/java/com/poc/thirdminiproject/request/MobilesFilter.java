package com.poc.thirdminiproject.request;

import lombok.Data;

@Data
public class MobilesFilter {

    String brand;
    Integer ram;
    Integer rating;
    Integer price;


}
