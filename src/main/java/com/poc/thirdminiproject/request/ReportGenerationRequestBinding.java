package com.poc.thirdminiproject.request;

import com.poc.thirdminiproject.pojos.MobileInfo;
import lombok.Data;

import java.util.List;

@Data
public class ReportGenerationRequestBinding {
    List<MobileInfo> mobileCatalog;
}
