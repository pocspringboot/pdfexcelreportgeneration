package com.poc.thirdminiproject.services;

import com.poc.thirdminiproject.entitys.Mobile;
import com.poc.thirdminiproject.repos.ProductRepo;
import com.poc.thirdminiproject.request.MobilesFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MobileInfoService {

    @Autowired
    ProductRepo productRepo;

    public List<Mobile> getMobilesBasedOnFilters(MobilesFilter mobilesFilter) {

        return productRepo.getMobiles(mobilesFilter.getBrand(), mobilesFilter.getRam(), mobilesFilter.getPrice(), mobilesFilter.getRating());

    }

    public List<Mobile> getAllMobiles() {
        return productRepo.findAll();
    }

    public List<String> getBrands() {

        return productRepo.getBrands();

    }

    public List<Integer> getRams() {

        return productRepo.getRams();

    }



    public List<Integer> getRatings() {

        return productRepo.getRatings();

    }
}
