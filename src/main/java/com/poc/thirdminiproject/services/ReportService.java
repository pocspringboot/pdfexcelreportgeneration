package com.poc.thirdminiproject.services;

import com.poc.thirdminiproject.excpetions.ReportGenerationException;
import com.poc.thirdminiproject.pojos.MobileInfo;
import com.poc.thirdminiproject.utils.ExcelReportGen;
import com.poc.thirdminiproject.utils.PdfReportGen;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {

   public byte[] generatePdfReport( List<MobileInfo> mobileInfo) throws ReportGenerationException {
       return PdfReportGen.generatePdfReport(mobileInfo);
   }

    public byte[] generateExcelReport( List<MobileInfo> mobileInfo) throws ReportGenerationException {
        return ExcelReportGen.excelReportGeneration(mobileInfo);
    }
}
