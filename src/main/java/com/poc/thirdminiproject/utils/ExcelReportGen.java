package com.poc.thirdminiproject.utils;

import com.poc.thirdminiproject.excpetions.ReportGenerationException;
import com.poc.thirdminiproject.pojos.MobileInfo;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ExcelReportGen {

    public static byte[] excelReportGeneration(List<MobileInfo> catalog) throws ReportGenerationException {
        String[] COLUMNNs = {"NAME", "BRAND", "RAM", "PRICE"};
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet("Mobile Catalog");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.DARK_GREEN.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            // CellStyle for Age
            CellStyle ageCellStyle = workbook.createCellStyle();
            ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));

            AtomicInteger rowIdx = new AtomicInteger(1);
            catalog.forEach(item -> {

                Row row = sheet.createRow(rowIdx.getAndIncrement());

                row.createCell(0).setCellValue(item.getName());
                row.createCell(1).setCellValue(item.getBrand());
                row.createCell(2).setCellValue(item.getRam());
                row.createCell(3).setCellValue(item.getPrice());


            });

            workbook.write(out);
            return out.toByteArray();
        }
        catch (Exception e){
            throw  new ReportGenerationException(e.getMessage());
        }
    }
}
