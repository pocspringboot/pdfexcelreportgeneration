package com.poc.thirdminiproject.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.poc.thirdminiproject.excpetions.ReportGenerationException;
import com.poc.thirdminiproject.pojos.MobileInfo;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.List;

public class PdfReportGen {

    public static byte[] generatePdfReport(List<MobileInfo> mobileInfo) throws ReportGenerationException {

        try {
            ByteArrayOutputStream file = new ByteArrayOutputStream();
            Document document = new Document();
            PdfWriter.getInstance(document, file);

            document.open();
            document.add(new Paragraph("PDF Reporting Demo"));
            document.add(new Paragraph(LocalDate.now().toString()));

            document.addCreationDate();
            document.addCreator("Anshul");

            //Create Paragraph
            Paragraph paragraph = new Paragraph("Mobiles", new Font(Font.FontFamily.TIMES_ROMAN, 18,
                    Font.BOLD));

            //New line


            paragraph.add(new Paragraph(" "));
            document.add(paragraph);

            //Create a table in PDF
            PdfPTable pdfTable = new PdfPTable(4);
            PdfPCell cell1 = new PdfPCell(new Phrase("Name"));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("BRAND"));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("RAM"));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);

            cell1 = new PdfPCell(new Phrase("PRICE"));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfTable.addCell(cell1);
            pdfTable.setHeaderRows(1);

            mobileInfo.forEach(item -> {

                        pdfTable.addCell(item.getName());
                        pdfTable.addCell(item.getBrand());
                        pdfTable.addCell(item.getRam());
                        pdfTable.addCell(item.getPrice());


                    }
            );

            document.add(pdfTable);
            document.close();
            return file.toByteArray();


        } catch (Exception e) {

            throw new ReportGenerationException(e.getMessage());
        }
    }
}
