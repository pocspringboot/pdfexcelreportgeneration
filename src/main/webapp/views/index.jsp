<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>


<head>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <title>Mobiles</title>


    <script>
        $(document).ready(function () {
            $('#myTable').DataTable();
        });
    </script>
</head>


<body>

<h1>Mobiles Catalog</h1>


<div align="center">
    <form:form action="searchMobiles" modelAttribute="formObj" method="post">

        <table>
            <tr>
                <td>  &nbsp;&nbsp; Brand &nbsp;&nbsp;</td>
                <td>
                    <form:select path="brand">
                        <form:option value="">---Select---</form:option>

                        <form:options items="${brands}"/>
                    </form:select>
                </td>

                <td>      &nbsp;&nbsp; Ratings &nbsp;&nbsp;  </td>

                <td>
                    <form:select path="rating">
                        <form:option value="">---Select---</form:option>

                        <form:options items="${ratings}"/>
                    </form:select>
                </td>
                <td>  &nbsp;&nbsp; Ram Size &nbsp;&nbsp;</td>

                <td>
                    <form:select path="ram">
                        <form:option value="">---Select---</form:option>

                        <form:options items="${rams}"/>
                    </form:select>
                </td>

                <td> &nbsp;&nbsp; Price &nbsp;&nbsp;</td>

                <td>
                    <form:select path="price" >
                        <form:option value="">---Select---</form:option>

                        <form:option value="10000"><10000</form:option>
                        <form:option value="20000"><20000</form:option>
                        <form:option value="30000"><30000</form:option>

                        <form:option value="40000"><40000</form:option>

                        <form:option value="50000"><50000</form:option>
                        <form:option value="60000"><60000</form:option>

                        <form:option value="70000"><70000</form:option>


                    </form:select>
                </td>

                <td> &nbsp;&nbsp; <input type="submit" value="Search"></td>
            </tr>
        </table>
    </form:form>

</div>


<div align="center">

    <table id="myTable" class="table table-striped table-bordered" style="width: 80% ">
        <thead>
        <tr>
            <th bgcolor="#ff6347">BRAND</th>
            <th bgcolor="#ff6347">MODEL NAME</th>
            <th bgcolor="#ff6347">RAM</th>
            <th bgcolor="#ff6347">PRICE</th>
            <th bgcolor="#ff6347">RATINGS</th>

        </tr>


        </thead>
        <tbody>

        <c:forEach items="${mobileList}" var="mobile">
            <tr>

                <td><c:out value="${mobile.brand}"/></td>

                <td><c:out value="${mobile.name}"/></td>

                <td><c:out value="${mobile.ram}"/></td>

                <td><c:out value="${mobile.price}"/></td>

                <td><c:out value="${mobile.ratings}"/></td>
            </tr>

        </c:forEach>
        </tbody>
    </table>


</div>


<div align="center">

    <form>

        <input type="submit" value="EXPORT RESULT  TO PDF"/>
        <input type="submit" value="EXPORT RESULT TO EXCEL"/>

    </form>

</div>


</body>
</html>
